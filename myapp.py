from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from handler import ReadHandler
from typing import Optional
from pydantic import BaseModel

app = FastAPI()


class Schema1(BaseModel):
    name: str
    phonenumber: Optional[int]


@app.get("/")
async def introduction():
    return [{"msg": "Intro"}]


@app.get("/hello")
async def hello():
    return [{"message": "Hello!!!"}]


@app.get("/world")
async def world():
    return [{"message": "World"}]


@app.post("/task1")
async def excel_to_json1():
    json_data_final = ReadHandler.excel_to_json()
    print(json_data_final)
    return json_data_final


@app.post("/practice")
def details_printing(request_data: Schema1):
    return request_data.name


def custom_openapi():
    # cache the generated schema
    if app.openapi_schema:
        return app.openapi_schema

    # custom settings
    openapi_schema = get_openapi(
        title="Sample Redoc Task",
        version="1.0",
        description="Redoc is an open-source tool that generates API documentation from OpenAPI specifications. It's one of the most powerful free docs tools in the industry, producing clean, customizable documentation with an attractive three-panel design. With support for Markdown, it allows you to write and style descriptions with ease.",
        routes=app.routes,
    )

    # logo

    openapi_schema["info"]["x-logo"]= {"url": "https://www.ilmexhibitions.com/cemindia/exhibitor/logos/KL%20Logo.png"}

    # path for hello
    openapi_schema["paths"]["/hello"]["get"]["x-codeSamples"] = [{
        'lang': 'JavaScript',
        'source': 'console.log("hello");',
        'label': 'Plain JS'
    }]
    # path for task1
    openapi_schema["paths"]["/task1"]["post"]["x-codeSamples"] = [{
        'lang': 'JavaScript',
        'source': 'console.log(json_data_final);',
        'label': 'Plain JS'
    }]

    # path for practice
    openapi_schema["paths"]["/practice"]["post"]["x-codeSamples"] = [{
        'lang': 'Python',
        'source': 'System.out.println("Done");',
        'label': 'Plain JS'
    }]
    app.openapi_schema = openapi_schema

    return app.openapi_schema


# assign the customized OpenAPI schema
app.openapi = custom_openapi
