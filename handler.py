import pandas as pd
from fastapi import HTTPException


class ReadHandler:

    @staticmethod
    def excel_to_json():
        df = pd.read_excel(r'C:\Users\mamatha\datafile1.xlsx')
        json_data = df.to_dict('records')
        if bool(json_data) is True:
            final_json = {"Persons": json_data}
            return final_json
        else:
            raise HTTPException(status_code=404, detail="No data found")
